import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, accuracy_score, precision_score, recall_score


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
#a
plt.figure()
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, label='Training data',cmap='bwr')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, label='Test data',cmap='bwr', marker='x')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()

#b
logisticModel=LogisticRegression()
logisticModel.fit(X_train, y_train)

#c
b=logisticModel.intercept_[0] #theta0
w1,w2=logisticModel.coef_.T #theta1,theta2
c=-b/w2
m=-w1/w2

xmin, xmax = -4, 4
ymin, ymax = -4, 4

xd=np.array([xmin, xmax])
yd=m*xd+c
plt.figure()
plt.plot(xd, yd, 'b',lw=1,ls='--')
plt.fill_between(xd, yd, ymin, color='tab:orange', alpha=0.2)
plt.fill_between(xd, yd,ymax, color='tab:blue', alpha=0.2)

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, label='Training data',cmap='bwr')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, label='Test data',cmap='bwr', marker='x')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()

#d
y_test_p=logisticModel.predict(X_test)

cm=confusion_matrix(y_test, y_test_p)
disp=ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_p))
disp.plot()

print("Accuracy: ", accuracy_score(y_test, y_test_p))
print("Precision: ", precision_score(y_test, y_test_p))
print("Recall: ", recall_score(y_test, y_test_p))


#e
plt.figure()
colors=['black', 'green']
plt.plot(xd, yd, 'b',lw=1,ls='--')
plt.scatter(X_test[:,0], X_test[:,1], c=y_test_p==y_test, cmap=matplotlib.colors.ListedColormap(colors))
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()