import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

colors=np.unique(img_array_aprox, axis=0)
print(f"Broj boja u slici je {len(colors)}.")

km=KMeans(n_clusters=5, init='random', n_init=5)
km.fit(img_array_aprox)
labels=km.predict(img_array_aprox)
qColors=km.cluster_centers_[labels]
qImg=np.reshape(qColors,(w,h,d))

plt.figure()
plt.title("Kvantizirana slika")
plt.imshow(qImg)
plt.tight_layout()

inertia_values=[]
n_clusters=range(1,10)
for i in n_clusters:
    km=KMeans(n_clusters=i, init='random', n_init=5)
    km.fit(img_array_aprox)
    inertia_values.append(km.inertia_)

plt.figure()
plt.plot(n_clusters, inertia_values, marker='o')
plt.xlabel('Broj grupa K')
plt.ylabel('Kriterijska funkcija J')
plt.title('Ovisnost J o broju grupa K')
plt.show()