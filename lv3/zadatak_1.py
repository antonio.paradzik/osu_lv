import pandas as pd
import numpy as np

data=pd.read_csv('data_C02_emission.csv')

print(len(data))
print(data.info())
print(data.isnull().sum())
print(data.duplicated())
data.dropna(axis=0)
data.dropna(axis=1)
data.drop_duplicates()
data = data.reset_index( drop = True )

data_lst = data.columns[data.dtypes == "object"].tolist()
for element in data_lst:
    data[element] = data[element].astype("category")
print(data.info())
consumption=data.sort_values(by="Fuel Consumption City (L/100km)", ascending=False)
print(data.head(3)[['Make','Model','Fuel Consumption City (L/100km)']])
print(data.tail(3)[['Make','Model','Fuel Consumption City (L/100km)']])

engine_size=data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(len(engine_size))
print(engine_size['CO2 Emissions (g/km)'].mean())

audi=data[(data["Make"] == "Audi")]
print(len(audi))
print(audi[audi.Cylinders == 4]["CO2 Emissions (g/km)"].mean())

cylinders=data[(data.Cylinders >= 4) & (data.Cylinders % 2 == 0)]
print(len(cylinders))
emission_by_cylinders=cylinders.groupby("Cylinders")
print(emission_by_cylinders["CO2 Emissions (g/km)"].mean())

diesel=data[data["Fuel Type"] == "D"]
regular_gasoline=data[data["Fuel Type"] == "X"]
print(diesel["Fuel Consumption City (L/100km)"].mean())
print(regular_gasoline["Fuel Consumption City (L/100km)"].mean())
print(diesel["Fuel Consumption City (L/100km)"].median())
print(regular_gasoline["Fuel Consumption City (L/100km)"].median())

most_consumption=data[(data["Cylinders"] == 4) & (data["Fuel Type"] == "D")]
print(most_consumption["Fuel Consumption City (L/100km)"].max())

print(len(data[data["Transmission"].str.startswith("M")]))

print(data.corr(numeric_only=True))
print(data["Cylinders"])

