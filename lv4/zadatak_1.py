import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error,mean_squared_error, mean_absolute_percentage_error,r2_score

data=pd.read_csv("data_C02_emission.csv")

input_variables = [
    'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
    'Engine Size (L)',
    'Cylinders'
]

output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
Y = data[output_variable].to_numpy()

X_train, X_test, y_train, y_test =train_test_split(X, Y, test_size = 0.2, random_state=1)

plt.figure()
plt.scatter(X_train[:,0], y_train, color='blue', label='Training data', s=7)
plt.scatter(X_test[:,0], y_test, color='red', label='Test data', s=7)
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()

sc=StandardScaler()
X_train_n=sc.fit_transform(X_train)
X_test_n=sc.transform(X_test)

plt.figure()
plt.subplot(2,1,1)
plt.hist(X_train[:,0])
plt.title('Before standardization')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')

plt.subplot(2,1,2)
plt.hist(X_train_n[:,0])
plt.title('After standardization')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('Frequency')


linearModel=lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.intercept_)
print(linearModel.coef_)

y_test_p=linearModel.predict(X_test_n)
plt.figure()
plt.scatter(X_test[:,0], y_test, color='blue', label='Real values', s=7)
plt.scatter(X_test[:,0], y_test_p, color='red', label='Predicted values', s=7)
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()


MSE=mean_squared_error(y_test,y_test_p)
MAE=mean_absolute_error(y_test, y_test_p)
RMSE=np.sqrt(MSE)
MAPE=mean_absolute_percentage_error(y_test, y_test_p)
R2=r2_score(y_test, y_test_p)
print(f"MSE: {MSE}, MAE: {MAE}, RMSE: {RMSE}, MAPE: {MAPE}, R2: {R2}" )
plt.show()
