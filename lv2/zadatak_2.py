import numpy as np
import matplotlib.pyplot as plt

data=np.loadtxt("data.csv" , skiprows=1, delimiter=",")
print(len(data))
plt.scatter(data[:,1], data[:,2], s=1, marker=".")
plt.figure()

data50=data[::50]
plt.scatter(data50[:,1], data50[:,2],s=1, c="b")

print(data[:,1].min())
print(data[:,1].max())
print(data[:,1].mean())
print("\n")
m=(data[:,0]==1)
f=(data[:,0]==0)
m_data=data[m]
f_data=data[f]

print(m_data[:,1].min())
print(m_data[:,1].max())
print(m_data[:,1].mean())
print("\n")
print(f_data[:,1].min())
print(f_data[:,1].max())
print(f_data[:,1].mean())

plt.show()