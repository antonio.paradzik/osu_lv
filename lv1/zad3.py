lst=[]


while True:
    x=input('Enter a number or Done to end a program: ')
    if x=='Done':
        break
    try:
        number=float(x)
        lst.append(number)

    except ValueError:
        print('Input must be a number')

def mean(lst):
    for number in lst:
        sum=0
        sum+=number
        return sum/len(lst)
    
print('You entered', len(lst), 'numbers')
print('Mean is ', mean(lst))
print('Min value is', min(lst))
print('Max value is ', max(lst))
print(sorted(lst))

