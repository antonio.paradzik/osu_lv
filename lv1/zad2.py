try:
    num=float(input('Enter a grade between 0.0 and 1.0: '))
    if num >= 0.0 and num<= 1.0:
        if num>=0.9:
            print('A grade')
        elif num>=0.8:
            print('B grade')
        elif num>=0.7:
            print('C grade')
        elif num>=0.6:
            print('D grade')
        else:
            print('F grade')
    else:
        print('Number is not in wanted interval')
except ValueError:
    print('Input must be a number')