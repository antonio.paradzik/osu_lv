hams=[]
spams=[]
br=0

fhand=open('SMSSpamCollection.txt')
for line in fhand :
    line = line.rstrip()
    words = line.split()
    if(words[0] == 'ham'):
        hams.append(len(words)-1)
    elif(words[0] == 'spam'):
        spams.append(len(words)-1)
        if line[len(line)-1] == "!":
            br+=1
fhand.close()

avgHam=sum(hams)/len(hams)
avgSpam=sum(spams)/len(spams)

print(f"Average number of words in hams: {avgHam}\nAverage number of words in spams: {avgSpam}. Number of exclamation marks: {br}")